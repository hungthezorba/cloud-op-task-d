const express = require("express");
const bodyParser = require("body-parser");
const methodOverride = require("method-override");
const app = express();

const Pool = require("pg").Pool;
const { SSMClient, GetParameterCommand } = require("@aws-sdk/client-ssm");
const ssmClient = new SSMClient({
	region: "us-east-1",
});

const getPgDb = new GetParameterCommand({
  Name: 'PostgresDatabase'
});
const getPgUser = new GetParameterCommand({
  Name: 'PostgresUser'
});
const getPgPass = new GetParameterCommand({
  Name: 'PostgresPass'
});
const getPgEndpoint = new GetParameterCommand({
  Name: 'PostgresEndpoint'
});

async function bootstrap() {
  const pgDb = ssmClient.send(getPgDb);
  const pgUser = ssmClient.send(getPgUser);
  const pgPass = ssmClient.send(getPgPass);
  const pgEndpoint = ssmClient.send(getPgEndpoint);

  const res = await Promise.all([pgDb, pgUser, pgPass, pgEndpoint])

	const database = res[0].Parameter.Value;
	const user = res[1].Parameter.Value;
	const password = res[2].Parameter.Value;
	const host = res[3].Parameter.Value;
  console.log({ database, user, password, host  });
	const pool = new Pool({
		database,
		user,
		password,
		host,
		port: 5432,
	});

	const getUsers = (request, response) => {
		pool.query("SELECT * FROM users ORDER BY id ASC", (error, results) => {
			if (error) {
				throw error;
			}
			// response.status(200).json(results.rows)
			response.render("index.ejs", { users: results });
		});
	};

	const getUserById = (request, response) => {
		const id = parseInt(request.params.id);

		pool.query("SELECT * FROM users WHERE id = $1", [id], (error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).json(results.rows);
		});
	};

	const createUser = (request, response) => {
		const { name, email } = request.body;

		pool.query(
			"INSERT INTO users (name, email) VALUES ($1, $2) RETURNING id",
			[name, email],
			(error, results) => {
				if (error) {
					throw error;
				}
				// response.status(201).send(`User added with ID: ${results.rows[0].id} ${JSON.stringify(request.body)}`)
				console.log(request.body);
				response.redirect("/");
			}
		);
	};

	const updateUser = (request, response) => {
		const id = parseInt(request.params.id);
		const { name, email } = request.body;

		pool.query(
			"UPDATE users SET name = $1, email = $2 WHERE id = $3",
			[name, email, id],
			(error, results) => {
				if (error) {
					throw error;
				}
				// response.status(200).send(`User modified with ID: ${id}`)
				response.redirect("/");
			}
		);
	};

	const deleteUser = (request, response) => {
		const id = parseInt(request.params.id);

		pool.query("DELETE FROM users WHERE id = $1", [id], (error, results) => {
			if (error) {
				throw error;
			}
			// response.status(200).send(`User deleted with ID: ${id}`)
			response.redirect("/");
		});
	};

	const port = 3000;

	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true }));

	app.use(express.static(__dirname)); // serve static CSS file
	app.use(methodOverride("_method")); // support PUT/DELETE in forms
	app.set("view engine", "ejs"); // allow for dynamic pages

	app.get("/", getUsers);
	app.get("/users", getUsers);
	app.get("/users/:id", getUserById); // raw json
	app.post("/users", createUser);
	app.put("/users/:id", updateUser);
	app.delete("/users/:id", deleteUser);

	app.listen(port, () => {
		console.log(`App running on port ${port}.`);
	});
}

bootstrap();